#!/usr/bin/env python3

import socket
import time


class IrcClient(object):

    def __init__(self, host, port, nick, ident, realname, channels_to_join=None):
        self.host = host
        self.port = port
        self.nick = nick
        self.ident = ident
        self.realname = realname
        self.channels_to_join = channels_to_join
        
        self._socket = socket.socket()

    def _connect(self):
        attempts = 3
        while attempts > 0:
            try:
                self._socket.connect((self.host, self.port))
                self.send('NICK {}'.format(self.nick))
                self.send('USER {} {} foo :{}'.format(self.ident, self.host, self.realname))
                break
            except (BrokenPipeError, OSError):
                attempts -= 1
                time.sleep(1)

    def _disconnect(self):
        self._socket.close()

    def _reconnect(self):
        self._disconnect()
        self._connect()

    def send(self, string):
        """
        :type string: str
        """
        print('-> ' + string)
        def send():
            self._socket.send(bytes(string + '\r\n', 'UTF-8'))

        try:
            send()
        except (BrokenPipeError, OSError):
            while True:
                try:
                    self._reconnect()
                    send()
                except OSError:
                    time.sleep(1)
                    pass


    def privmsg(self, recipient, message):
        self.send('PRIVMSG {} :{}'.format(recipient, message))

    def join(self, channel):
        self.send('JOIN {}'.format(channel))

    def part(self, channel):
        self.send('PART {}'.format(channel))

    def change_nick(self, new_nick):
        self.send('NICK {}'.format(new_nick))

    def loop(self):
        self._connect()
        readbuffer = ''
        while True:
            try:
                readbuffer = readbuffer + self._socket.recv(1024).decode('UTF-8')
            except UnicodeDecodeError as e:
                print(e)
                continue

            temp = str.split(readbuffer, '\n')
            readbuffer = temp.pop()

            for line in temp:
                print('<- {}'.format(line))

                if line.startswith('ERROR'):
                    self._reconnect()
                    continue

                line = str.rstrip(line)
                line = str.split(line)

                if line[0] == 'PING':
                    self.send('PONG {}'.format(line[1]))

                if line[1] == '376':  # End of MOTD.
                    for channel in self.channels_to_join:
                        self.join(channel)

                if line[1] == 'PRIVMSG':
                    pass
                    # message, sender = self._parse_privmsg(line)
                    # Echo
                    # self.privmsg(sender, message)


    @staticmethod
    def _parse_privmsg(line):
        sender = ''
        for char in line[0]:
            if char == '!':
                break
            if char != ':':
                sender += char
        size = len(line)
        i = 3
        message = ''
        while i < size:
            message += line[i] + ' '
            i = i + 1

        return message.lstrip(':'), sender

