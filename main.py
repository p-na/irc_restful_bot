import cherrypy
import threading
import configparser
import json
from irc_client import IrcClient


irc_conf = configparser.ConfigParser()
irc_conf.read('irc.conf')


irc = IrcClient(irc_conf['irc_client']['host'],
                int(irc_conf['irc_client']['port']),
                irc_conf['irc_client']['nick'],
                irc_conf['irc_client']['ident'],
                irc_conf['irc_client']['realname'],
                json.loads(irc_conf['irc_client']['channels']))


class Template(object):

    def __init__(self, name):
        with open('templates/{}.html'.format(name), 'r') as template:
            self.content = template.read()

    def __str__(self):
        return self.content

    def render(self):
        return self.content


class Message(object):

    @cherrypy.expose
    def index(self):
        return Template('help').render()

    @cherrypy.expose('msg')
    @cherrypy.tools.json_out()
    def message(self, recipient, message):
        """
        :type recipient: str
        :type message: str
        """
        if recipient and message:
            irc.privmsg(recipient, message)
        else:
            cherrypy.response.status = '400 Bad Request'
            return {'error': 'Invalid arguments'}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def join(self, channel):
        """
        :type channel: str
        """
        if channel:
            irc.join(channel)
        else:
            return {'error': 'Invalid arguments'}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def part(self, channel):
        """
        :type channel: str
        """
        if channel:
            irc.part(channel)
        else:
            return {'error': 'Invalid arguments'}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def nick(self, new_nick):
        """
        :type new_nick: str
        """
        if new_nick:
            irc.change_nick(new_nick)
        else:
            return {'error': 'Invalid arguments'}


def start_cherrypy_server():
    cherrypy.config.update('cherrypy.conf')
    cherrypy.quickstart(Message(), '', {})


threading.Thread(target=lambda: irc.loop()).start()
threading.Thread(target=start_cherrypy_server).start()

